import sys
import re

passports = []
p = dict()


def toBinary(s, oneChar):
  val = 0
  for char in s:
    val*=2
    if char == oneChar:
      val+=1
  return val

highestNum = 0
allSeats = set(range(880))


for line in sys.stdin:
  line = line.strip()
  fbs = line[:-3]
  lrs = line[-3:]
  
  fb_int = toBinary(fbs, 'B')
  lr_int = toBinary(lrs, 'R')

  seat_id = fb_int * 8 + lr_int
  

  if seat_id in allSeats:
    allSeats.remove(seat_id)
  else:
    print('missingSeat', seat_id)

  if seat_id > highestNum:
    highestNum = seat_id
    print(line,fbs,fb_int, lrs, lr_int, seat_id)
print(allSeats)
