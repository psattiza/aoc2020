import sys
import re

pattern = re.compile("([0-9]*)\-([0-9]*) ([a-z]): ([a-z]*)")

valid = 0


trees = [line.strip() for line in sys.stdin]

n = len(trees)
m = len(trees[0])
treeHits = 0
product = 1
#print(n,m)

slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]
for slope in slopes:
  for i in range(0,n,slope[1]):
    #print("   -- ",i, slope[1], trees[i], (i*slope[0]//slope[1]) % m)
    # print( i, trees[i])
    if trees[i][(i*slope[0]//slope[1]) % m] == '#':
      treeHits += 1
      #print("hit")
  product *= treeHits
  #print(product, treeHits)
  treeHits = 0
print(product)
