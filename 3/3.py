import sys
import re

pattern = re.compile("([0-9]*)\-([0-9]*) ([a-z]): ([a-z]*)")

valid = 0


trees = [line.strip() for line in sys.stdin]

n = len(trees)
m = len(trees[0])
treeHits = 0
#print(n,m)

for i in range(n):
  #print( i, trees[i])
  if trees[i][(i*3) % m] == '#':
    treeHits += 1
    #print("hit")

print(treeHits)
