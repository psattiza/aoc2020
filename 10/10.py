import sys
import re

valid = 0

lines = [int(line.strip()) for line in sys.stdin]

lines.append(0)
lines.append(max(lines)+3)

sorted_lines = sorted(lines)


counts = dict()

counts[1] = 0
counts[2] = 0
counts[3] = 0

for i in range(len(sorted_lines)-1):
    a = sorted_lines[i]
    b = sorted_lines[i+1]
    difference = b-a
    # print(difference)

    counts[difference] += 1

    # if(difference not in counts):
    #print("***", difference)

print(counts, counts[1]*counts[3])
