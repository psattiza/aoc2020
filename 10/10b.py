import sys
import re
from functools import lru_cache

valid = 0

lines = [int(line.strip()) for line in sys.stdin]

lines.append(0)
lines.append(max(lines)+3)

sorted_lines = sorted(lines)

reversed_sorted = list(reversed(sorted_lines))


@lru_cache()
def count_choices(rsList):
    # print("b", rsList)
    if len(rsList) == 2:
        # print('baseA')
        return 1
        # reVal = set()
        # reVal.add(rsList)
        # return reVal

    if len(rsList) == 0 or len(rsList) == 1:
        if len(rsList) == 0:
            pass
            # print("????")
        return 1
        # reVal =1 set()
        # reVal.add(rsList)
        # return reVal
    current_charger = rsList[0]
    choices = rsList[1:]

    valid_jumpLocations = set()
    for jump_size in range(1, 3+1):
        valid_jumpLocations.add(current_charger - jump_size)

    ways = 0
    for i in range(3):
        if i < len(choices) and choices[i] in valid_jumpLocations:
            ways += count_choices(choices[i:])
            # for sub_way in recursiveCall:
            #     cc = list(sub_way)
            #     #print(" ccx:", cc)
            #     cc.append(current_charger)
            #     # print(" ccxxx:", cc)
            #     newWay = tuple(cc)
            #     ways.add(newWay)

    return ways


collect = set()
choice = count_choices(tuple(reversed_sorted))
print(choice)
