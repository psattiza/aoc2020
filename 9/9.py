import sys
import re

valid = 0

passports = []
p = dict()


lines = [int(line.strip()) for line in sys.stdin]

window_size = 25
i = 0

magic_number =


def check_window(win):
    valid_sums = set()
    for i in range(window_size):
        for j in range(i+1, window_size):
            valid_sums.add(win[i]+win[j])

    if win[-1] not in valid_sums:
        print(win, "found to be invalid", win[-1])
        return False
    return True


while i + window_size+1 <= len(lines):
    window = lines[i:i+window_size+1]

    # check if window is valid
    if not check_window(window):
        print("DUNZO")
        exit()
    i += 1

print("SADZO")
