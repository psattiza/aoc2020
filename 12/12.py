import sys
import copy
import math
lines = [  line.strip() for line in sys.stdin]

commands = [ (part[0], int(part[1:])) for part in lines]

print(commands)

x=0 #east
y=0 #north

heading = 0 # polar for eazy cos sin, start eastward ccw+, left+ 

for command in commands:
    instruction = command[0]
    n = command[1]
    if instruction == 'N' :
        y+=n
    elif instruction == 'S':
        y-=n
    if instruction == 'E' :
        x+=n
    elif instruction == 'W':
        x-=n
    if instruction == 'L' :
        heading+=n
    elif instruction == 'R':
        heading-=n
    elif instruction == 'F':
        heading_in_rad = heading * (2 * math.pi ) / 360
        y+= math.sin(heading_in_rad) * n
        x+= math.cos(heading_in_rad) * n
    else:
        print("What in tarnations does:", instruction, "do?")
    print(x,y,heading)

print(abs(x)+abs(y))
    


