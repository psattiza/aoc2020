import sys
import copy
import math
lines = [  line.strip() for line in sys.stdin]

commands = [ (part[0], int(part[1:])) for part in lines]

print(commands)

x=0 #east
y=0 #north
wx=10 #waypoint x
wy=1 #waypoint y

heading = 0 # polar for eazy cos sin, start eastward ccw+, left+ 

for command in commands:
    instruction = command[0]
    n = command[1]
    if instruction == 'N' :
        wy+=n
    elif instruction == 'S':
        wy-=n
    elif instruction == 'E' :
        wx+=n
    elif instruction == 'W':
        wx-=n
    elif instruction == 'R' or instruction == 'L' :
        right = instruction == 'R'
        if (n == 90 and right) or (n == 270 and not right):
            tmp = wx
            wx = wy
            wy = -tmp
        elif n == 180:
            wx *= -1
            wy *= -1
        elif (right and n == 270) or ( not right and n == 90):
            tmp = wx
            wx = -wy
            wy = tmp
    elif instruction == 'F':
        x+= wx * n
        y+= wy * n
    else:
        print("What in tarnations does:", instruction, "do?")
    print(command, "(",x,y,")",heading,"(", wx, wy,")")

print(abs(x)+abs(y))
    


