import sys
import re

valid = 0

passports = []
p = dict()


#nop +0
#acc +1
#jmp +4
#acc +3
#jmp -3
#acc -99
#acc +1
#jmp -4
#acc +6

program = dict()

lineNum = 0
for line in sys.stdin:
    s = line.strip().split(" ")
    command = s[0]
    val = int(s[1])
    program[lineNum] = (command, val)
    lineNum += 1


def runProgram(program):
    linesVisited = set()
    accumulator = 0
    pc = 0

    while pc not in linesVisited:
        linesVisited.add(pc)

        command, value = program[pc]
        print(command, value)

        if command == "nop":
            pc += 1
        elif command == "acc":
            accumulator += value
            pc += 1
        elif command == "jmp":
            pc += value
        else:
            print("What are this? What do?", command)

    print(accumulator)


runProgram(program)
