import sys
import re
import collections
from functools import lru_cache

passports = []
aContainsB = collections.defaultdict(list)
aContainedByB = collections.defaultdict(list)

for line in sys.stdin:
  line = line.strip().strip('.').split(" ")

  # blah bla bag contains ...

  bigBag = " ".join(line[:2])

  contentBags = []
  
  if line[4] == "no":
    continue
  else:
    contentBagWords = line[4:]
    while len(contentBagWords) > 0:
      numNextBag = int(contentBagWords[0])
      nextBag = " ".join(contentBagWords[1:3]).strip('.,')
      contentBags.append((numNextBag, nextBag))
      
      contentBagWords = contentBagWords[4:]
    #print(bigBag, "<-", contentBags)

    aContainsB[bigBag] = contentBags
    #for contentBag in contentBags:
    #  aContainedByB[contentBag].append(bigBag)

#print(aContainsB)
#print()
#for k,v in aContainedByB.items():
#  print(k, "->", v)
#visitedBags = set()
#bagsToVisit = set(["shiny gold"])
#
#bagsContainingGold = set()
#
#while len(bagsToVisit) > 0:
#  visiting = bagsToVisit.pop()
#  visitedBags.add(visiting)
#
#  for toVisit in aContainedByB[visiting]:
#    if toVisit not in visitedBags:
#      bagsContainingGold.add(toVisit)
#      bagsToVisit.add(toVisit)
#      print(toVisit)
#
#
#print(bagsContainingGold, len(bagsContainingGold))

@lru_cache
def bagsIn(bag):
  contents = aContainsB[bag]
  #print("bIn", bag, contents)

  return sum([ bagInfo[0]*(1+bagsIn(bagInfo[1])) for bagInfo in contents])

print(bagsIn("shiny gold"))
