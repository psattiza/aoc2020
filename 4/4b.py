import sys
import re

required_fields = [
"byr",
"iyr",
"eyr",
"hgt",
"hcl",
"ecl",
"pid" ]
# cid not required 

# byr (Birth Year) - four digits; at least 1920 and at most 2002.
# iyr (Issue Year) - four digits; at least 2010 and at most 2020.
# eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
# hgt (Height) - a number followed by either cm or in:
#   If cm, the number must be at least 150 and at most 193.
#   If in, the number must be at least 59 and at most 76.
# hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
# ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
# pid (Passport ID) - a nine-digit number, including leading zeroes.

valid = 0

passports = []
p = dict()

for line in sys.stdin:
  s = line.strip()
  if len(s) == 0 :
    #print("new passport")
    passports.append(p)
    p = dict()
  else:
    s = s.split()
    for field in s:
      parts = field.split(':')
      p[parts[0]] = parts[1]
      # print(p)
passports.append(p)

#print(passports)
height_pattern = re.compile("^([0-9]+)(cm|in)$")
color_pattern = re.compile("^#[0-9a-f]{6}$")
num_pattern= re.compile("^[0-9]{9}$")
valid_eye_colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]

valid = 0
for passport in passports:
  hasAll = True
  for field in required_fields:
    if field not in passport:
      hasAll = False
      continue
    val = passport[field]
    if field == "byr":
      val = int(val)
      if 1920 > val or  2002 < val:
        hasAll = False
    if field == "iyr":
      val = int(val)
      if 2010 > val or 2020 < val:
        hasAll = False
    if field == "eyr":
      val = int(val)
      if 2020 > val or 2030 < val:
        hasAll = False
    if field == "hgt":
      m = height_pattern.match(val)
      if not m:
        hasAll = False
      else:
        heightNum = int(m.group(1))
        if m.group(2) == "cm":
          if heightNum < 150 or heightNum > 193:
            hasAll = False
        else:
          if heightNum < 59 or heightNum > 76:
            hasAll = False
    if field == "hcl":
      m = color_pattern.match(val)
      if not m:
        hasAll = False
    if field == "ecl":
      if val not in valid_eye_colors:
        hasAll = False
    if field == "pid":
      m = num_pattern.match(val)
      if not m:
        hasAll = False
  if hasAll:
    valid +=1
    #print(passport)

print(valid)


